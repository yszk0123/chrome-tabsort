Chrome で開いているタブが増えてきた時にウィンドウを(半)自動分割する Chrome 拡張です。

以下の画像のように、URL に含まれる文字列を書き並べてドラッグ＆ドロップで配置するだけで設定完了です。

ウィンドウ右上のアイコンをクリックすると、分割が実行されます。

<スクリーンショットがほしい>

試験的機能ですが、開いているタブが一定数を超えた時に、分割を自動実行することも可能です。

# Develop

## Install

`$ npm install`

## Test

`$ npm test`

## Build

Build with development settings

`$ npm build`

Build with development settings (watch)

`$ npm watch`

Build with production settings

`$ npm release`
